function getCurrentTime(utcStr){
		console.log(utcStr[0])
		var hour = utcStr[0].slice(4, 6);
		var minute = utcStr[0].slice(7, 9);
		console.log(hour);
		console.log(minute);
		inputTzOffset=parseInt(hour);
		var now = new Date(); // get the current time

    var currentTzOffset = -now.getTimezoneOffset() / 60 // in hours, i.e. -4 in NY
    var deltaTzOffset = inputTzOffset - currentTzOffset; // timezone diff

    var nowTimestamp = now.getTime(); // get the number of milliseconds since unix epoch 
    var deltaTzOffsetMilli = deltaTzOffset * 1000 * 60 * 60; // convert hours to milliseconds (tzOffsetMilli*1000*60*60)
    var outputDate = new Date(nowTimestamp + deltaTzOffsetMilli) // your new Date object with the timezone offset applied.
    var dd=outputDate.getDate();
    var yyyy=outputDate.getFullYear();
    var month = new Array();
    month[0] = "Jan";
    month[1] = "Feb";
    month[2] = "Mar";
    month[3] = "Apr";
    month[4] = "May";
    month[5] = "Jun";
    month[6] = "Jul";
    month[7] = "Aug";
    month[8] = "Sep";
    month[9] = "Oct";
    month[10] = "Nov";
    month[11] = "Dec";
    var MM = month[outputDate.getMonth()];
    

    return dd.toString()+" "+MM+" "+yyyy.toString()+" "+(outputDate.getHours()<10?"0"+outputDate.getHours():outputDate.getHours())+":"+outputDate.getMinutes();
	}
	function filterList(){
		
		var input, filter, ul, li, a, i, txtValue;
        input = document.getElementById('searchInput');
        filter = input.value.toLowerCase();
        ul = document.getElementById("country-container-section");
        li = ul.getElementsByClassName('country-container');
        let x=document.getElementsByClassName("country-with-detail-country-name");
        console.log(x.length)
        for(i=0;i<x.length;i++){
        	if(!x[i].innerHTML.toLowerCase().includes(filter)){
        		li[i].style.display="none";
        	}else{
        		li[i].style.display=""
        	}
        }

  // Loop through all list items, and hide those who don't match the search query
  

	
	}
	
		function getCountries(){
			var txt="";
			var ajaxRequest = new XMLHttpRequest();
			ajaxRequest.onreadystatechange = function(){
			
				if(ajaxRequest.readyState == 4){
					//the request is completed, now check its status
					if(ajaxRequest.status == 200){
						//console.log (ajaxRequest.responseText);
						var json = JSON.parse(ajaxRequest.responseText);
                        
                        for(x in json){
                        	
                        	var currency="";

                        	for(curr in json[x].currencies){
                        		if(curr<1){
                        			currency+=json[x].currencies[curr].name;
                        		}else{
                        			currency+=","+json[x].currencies[curr].name;
                        		}
                        		
                        	}
                        	


txt +='<div class="container shadow-sm bg-white country-container" >'+
'<div class="row li" ><div class="col-md-4">'+
						'<img src='+json[x].flag+' class="img-fluid" width="100%" height="100%">'+
					'</div>'+
					'<div class="col-md-8">'+
						'<div class="row">'+
							'<div class="col-md-12 country-with-detail-country-name">'+
							    json[x].name+	
							'</div>'+
							
						'</div>'+
						'<div class="row">'+
							'<div class="col-md-12 country-detail-container">Currency:'+currency+'</div>'+
						'</div>'+
						'<div class="row">'+
							'<div class="col-md-12 country-detail-container" >Current date and time: '+getCurrentTime(json[x].timezones)+'</div>'+
							
						'</div>'+
						'<div class="row">'+
							
							'<div class="col-md-6"  id="button-container">'+
								'<a href="https://www.google.co.in/maps/place/'+json[x].name+'">'+
								'<button type="button" class="btn btn-outline-primary btn-block show-button" >Show Map</button>'+
							'</a>'+
							'</div>'+
				
							'<div class="col-md-6" id="button-container">'+
								'<a href="detail.html?code='+json[x].alpha3Code+'">'+
									'<button type="button" class="btn btn-outline-primary btn-block show-button" >Show Detail</button>'+
								'</a>'+
								
							'</div>'+
						'</div>'+
					'</div>'+
				'</div></div>'; 

                        }
                        
                        document.getElementById("country-container-section").innerHTML = txt;
                        
                        
					}
					else{
						console.log("Status error: " + ajaxRequest.status);
					}
				}
				else{
					console.log("Ignored readyState: " + ajaxRequest.readyState);
				}
			
			
			}
			ajaxRequest.open('GET', 'https://restcountries.eu/rest/v2/all');
			ajaxRequest.send();
		}
		
	