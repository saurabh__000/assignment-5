	function getDetail(){
		var url=window.location.href
		var alpha3="";
		
		var name="code";
		name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
         if (!results) return null;
         if (!results[2]) return '';
              alpha3=decodeURIComponent(results[2].replace(/\+/g, ' '));
             

	     var flagTxt="";
			var ajaxRequest = new XMLHttpRequest();
			ajaxRequest.onreadystatechange = function(){
			
				if(ajaxRequest.readyState == 4){
					//the request is completed, now check its status
					if(ajaxRequest.status == 200){
						
						var json = JSON.parse(ajaxRequest.responseText);
                        
                        	renderText(json)
                        	for(bd in json.borders){
                        		console.log(json.borders[bd].toLowerCase());
                        		
                        	}   
                        	renderFlags(json.borders);                     
					}

					else{
						console.log("Status error: " + ajaxRequest.status);
					}
				}
				else{
					console.log("Ignored readyState: " + ajaxRequest.readyState);
				}
			
			
			}
			
			ajaxRequest.open('GET', "https://restcountries.eu/rest/v2/alpha/"+alpha3);
			ajaxRequest.send();

}
function renderFlags(borders){
	var borderTxt="";
	for(bd in borders){
		console.log(borders[bd]);
		borderTxt+='<div class="col-md-4 countries-flag">'+
					'<img src="https://restcountries.eu/data/'+borders[bd].toLowerCase()+'.svg" class="img-fluid">'+
				'</div>';
	}
	document.getElementById("neighbour-rows").innerHTML = borderTxt;
}

function renderText(json){
	var currency=""
                        var language="";
                        var timezones="";
                        var countryCode="";
	                           for(curr in json.currencies){
                        		if(curr<1){
                        			currency+=json.currencies[curr].name;
                        		}else{
                        			currency+=","+json.currencies[curr].name;
                        		}
                        		
                        	}
                        	for(lang in json.languages){
                        		if(lang<1){
                        			language+=json.languages[lang].name;
                        		}else{
                        			language+=","+json.languages[lang].name;
                        		}
                        		
                        	}
                        	for(tz in json.timezones){
                        		if(tz<1){
                        			timezones+=json.timezones[tz];
                        		}else{
                        			timezones+=","+json.timezones[tz];
                        		}
                        		
                        	}
                        	for(cc in json.callingCodes){
                        		if(cc<1){
                        			countryCode+=json.callingCodes[cc];
                        		}else{
                        			countryCode+=","+json.callingCodes[cc];
                        		}
                        		
                        	}
	txt ='<h4 class="country-name-heading">'+json.name+'</h4>'+
				'<div class="row">'+
					'<div class="col-md-5">'+
						'<img src="'+json.flag+'" class="img-fluid">'+
					'</div>'+
					'<div class="col-md-7" >'+
					    '<div class="row country-detail-container">'+
							'<div class="col-md-12">Native name: '+json.nativeName+'</div>'+
						'</div>'+
						'<div class="row country-detail-container">'+
							'<div class="col-md-12">Capital: '+json.capital+'</div>'+
						'</div>'+
						'<div class="row country-detail-container">'+
							'<div class="col-md-12" >Population: '+json.population+'</div>'+
							
						'</div>'+
						'<div class="row country-detail-container">'+
							'<div class="col-md-12" >Region: '+json.region+'</div>'+
							
						'</div>'+
						'<div class="row country-detail-container">'+
							'<div class="col-md-12" >Sub-region: '+json.subregion+'</div>'+
							
						'</div>'+
						'<div class="row country-detail-container">'+
							'<div class="col-md-12" >Area: '+json.area+' Km<sup>2</sup>></div>'+
							
						'</div>'+
						'<div class="row country-detail-container">'+
							'<div class="col-md-12" >Country Code: +'+countryCode+'</div>'+
							
						'</div>'+
						'<div class="row country-detail-container">'+
							'<div class="col-md-12" >Language: '+language+'</div>'+
							
						'</div>'+
						'<div class="row">'+
							'<div class="col-md-12 country-detail-container">Currencies: '+currency+'</div>'+
						'</div>'+
						'<div class="row">'+
							'<div class="col-md-12 country-detail-container" >Timezones: '+timezones+'</div>'+
							
						'</div>'+
						
						
					'</div>'+
				'</div>';
				document.getElementById("country-container").innerHTML = txt;
}

